package com.taskplanner;

import java.util.Arrays;
import java.util.Scanner;

public class TaskPlanner {

    public static String askTask(Scedule scedule, String day){
        Scanner in = new Scanner(System.in);
        System.out.println("Please, input new tasks for " + scedule.getDay(day));
        return in.nextLine();
    }
    public static boolean isPresentTask(String task){
        if(task == "noTask") {
            System.out.println("Sorry, I don't understand you, please try again.");
            return false;
        }
        return true;
    }

    public static void inputText(Scedule scedule){
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Please, input the day of the week: ");
            String[] words = in.nextLine().split(" ");
            switch (words.length){
                case 1:
                    if (words[0].equals("exit")) break;
                    if (!isPresentTask(scedule.getTask(words[0]))){
                        continue;
                    } else {
                        System.out.printf("Your tasks for %s is: %s \n", scedule.getDay(words[0]), scedule.getTask(words[0]));
                    }
                    break;
                case 2 :
                    if (!isPresentTask(scedule.getDay(words[1]))){
                        continue;
                    }
                    if (words[0].equals("change") || words[0].equals("reschedule")) {
                        scedule.setTask(words[1],askTask(scedule, words[1]));
                        continue;
                    } else {
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                    break;
                default :
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }


    public static void main(String[] args) {
        Scedule scedule = new Scedule(7,2);
        String[][] data = scedule.getData();
        scedule.initialData(data);
        System.out.println(Arrays.deepToString(scedule.getData()));
        inputText(scedule);
    }
}
